use serde_json::Value;
use std::error::Error;

const BASE_URI: &str = "https://api.github.com/repos/crev-dev/crev-proofs/forks";
const PER_PAGE: usize = 30;

fn main() -> Result<(), Box<dyn Error>> {
    let client = reqwest::blocking::Client::new();
    let mut page = 1;
    loop {
        let text = &client
            .get(BASE_URI)
            .header("Accept", "application/vnd.github.v3+json")
            .header("User-Agent", "Auto Crev Proofs")
            .query(&[("sort", "oldest")])
            .query(&[("per_page", PER_PAGE), ("page", page)])
            .send()?
            .text()?;
        let arr: Vec<Value> = serde_json::from_str(text)?;
        arr.iter().for_each(|val| {
            if let Some(Value::String(url)) = val.get("clone_url") {
                println!("{}", url);
            } else {
                eprintln!("clone_url not found; response: {:?}", val);
            }
        });

        if arr.len() < PER_PAGE {
            return Ok(());
        }
        page += 1;
    }
}
